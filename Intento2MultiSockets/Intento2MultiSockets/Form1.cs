﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net; //Se añade libreria
using System.Net.Sockets;//Se añade liberia 
using System.Text;
using System.Threading;//Se añade liberia
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Intento2MultiSockets
{
    public partial class Form1 : Form
    {
        //ConexionTcpServer -> #7 Creo el TcpEscuchado
        private TcpListener mTcpListener;
        //#8 Creo Hilo
        private Thread mThread;
        //#9 Creo una lista de mis Clientes
        private List<ConexionTcpServer> mTcpList = new List<ConexionTcpServer>();

        //#10 Creo un evento de detección de entrada y salida de clientes
        public delegate void CambioDeConexionEvenHandler (ConexionTcpServer oTcpClient);
        //#11 Los eventos para la E/S
        public event CambioDeConexionEvenHandler ClienteConectado;
        public event CambioDeConexionEvenHandler ClienteDesconectado;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //#12 Creo el puerto de escucha Harcodeado tanto la Ip como el Puerto
            EscucharClientes("192.168.0.72", 2000);

            //#15 Establezco el evento de conexión la conexión con el metodo ServerConexiónRecivida
            ClienteConectado += ServerConexionRecibida;

            //#19 Establezco el evento de Conexión cerrada con el client
            ClienteDesconectado += ServerConexionCerrada;

        }

        //#13 Creo el Metodo
        private void EscucharClientes(string host, int port)
        {
            //#14 a mí Tcp listener lo instancio con su Ip y Puert y lo inicio.
            mTcpListener = new TcpListener (IPAddress.Parse(host), port);
            mTcpListener.Start ();

            //#25 Ahora utilizo un hilo para mantener la escucha al cliente pero seguir trabajando en el hilo principal
            mThread = new Thread(AceptarClientes);
            mThread.Start ();

        }

        //# 16 Creo que el metodo de SerConexiónRecibida
        private void ServerConexionRecibida(ConexionTcpServer mTcpClient)
        {
            //#17 Bloqueo mi lista, para comprobar si ese cliente ya era parte de mi list, si no, lo añado
            lock(mTcpList)
                if(!mTcpList.Contains(mTcpClient))
                    mTcpList.Add(mTcpClient);

            //#18 Cambio el valo de la Label que contabiliza los clientes conectados
            Invoke(new Action(() => lblClientes.Text = string.Format("N° Clientes: "+ mTcpList.Count.ToString() +" Clientes")));
            MessageBox.Show("Cliente Conectado");
        }

        //#20 Creo el metodo para quitar un cliente de la lista
        private void ServerConexionCerrada(ConexionTcpServer mTcpClient)
        {
            //#21 Lockeo la lista
            lock(mTcpList)
                //#22 Compruebo que exista dentro de mí lista 
                if (mTcpList.Contains(mTcpClient))
                {
                    //#23 Busco el indice del cliente y lo remuevo con un RemoveAt
                    int numCliente = mTcpList.IndexOf(mTcpClient);
                    mTcpList.RemoveAt(numCliente);
                }
            //#24 Nuevamente cambio el valor de la Label
            Invoke(new Action(() => lblClientes.Text = string.Format("N° Clientes: {0} Clientes", mTcpList.Count)));

        }

        //#26 Creo la task de escuchar al cliente

        private void AceptarClientes()
        {
            //#27 Creo un bucle infinito 
            do
            {
                //#28 Con un Breack en el cath en caso de una exepción
                try
                {

                    //#29 Creo una Variable que queda a la espera de una conexión con un clientec devuelve un TcpClient
                    var TcpConexion = mTcpListener.AcceptTcpClient();
                    
                    //#30 Aviso cuando el cliente se desconecta, también es un punto para la comunicón con el cliente
                    var srvClient = new ConexionTcpServer(TcpConexion);

                    if(ClienteConectado!=null)
                    {
                        ClienteConectado(srvClient);
                    }

                }
                catch(Exception e)
                {
                    MessageBox.Show(e.Message.ToString());
                    break;
                }


            }while (true);


        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
