﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Intento2MultiSocketsCliente
{
    public partial class Form1 : Form
    {
        //ConexionTcpCliente -> #38 Creo que mi instancia de ConexionTcpCliente
        public static ConexionTcpCliente mTcpClienteF = new ConexionTcpCliente();

        //#39 Harcodeo mi Ip y Purto
        public static string host = "192.168.0.72";
        public static int port = 2000;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //#40 Compruebo si la conexión falla
            if(mTcpClienteF.Conectar(host, port))
            {
                MessageBox.Show("Te conectaste al servidor " + host);
            }
            else
            {
                MessageBox.Show("Erro al conectarte con el servidor " + host);
                return;
            }


        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }
    }
}
