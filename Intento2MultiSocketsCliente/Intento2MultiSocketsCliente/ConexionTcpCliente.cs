﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Intento2MultiSocketsCliente
{
    //#31 Creo mí clase public Conexión TcpCliete 
    public class ConexionTcpCliente
    {
        //#32 Creo mi TcpCliente
        public TcpClient mTcpClient {  get; set; }
        //#33 Creo mí hilo
        public Thread mThread { get; set; }

        //#34 un evento para la deconexión del cliente
        public delegate void ClienteDesconectadoEvenHandler();
        public event ClienteDesconectadoEvenHandler ClienteDesconectado;

        //35 Creo un metodo booleano de Conexión

        public bool Conectar(string host, int port)
        {
            //#36 Creo un Try catch para detectar la desconexión
            try
            {
                //#37 Establezco un TcpCliente y lo conecto --> Form1.cs
                mTcpClient = new TcpClient();
                mTcpClient.Connect(IPAddress.Parse(host), port);

                mThread = new Thread(Escuchar);
                mThread.Start();

                return true;
            }
            catch (Exception e) {
                return false;
            }

        }

        private void Escuchar()
        {
            do
            {

            }while (true);

            if (ClienteDesconectado != null)
                ClienteDesconectado();

        }
        

    }
}
